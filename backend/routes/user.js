const { request, response } = require("express");
const express = require("express")
const db = require("../db")
const utils = require("../utils")

const router = express.Router();

router.get('/', (request, response)=>{
    const query = `SELECT id, name, email, age FROM users`

    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error,result))
    })
})


router.post('/', (request, response)=>{
    const {name, email, age} = request.body

    const query = `INSERT INTO users
     (name, email, age)
     VALUES
     ('${name}', '${email}', '${age}')`

    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.put('/:id', (request, response)=>{
    const {id} = request.params
    const {name, email, age} = request.body

    const query = `UPDATE users SET
     name='${name}', 
     email='${email}', 
     age='${age}'
     WHERE id = ${id}`

    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.delete('/:id', (request, response)=>{
    const {id} = request.params
    const query = `DELETE FROM users WHERE id = ${id}`

    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error,result))
    })
})

module.exports = router