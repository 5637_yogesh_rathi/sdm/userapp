import './App.css';
import UserPage from './pages/user';
function App() {
  return (
    <div className="App">
      <div class="container">
        <div class="row">
          <div className="col">
            <UserPage/>
          </div>
        </div>
        
      </div>
      
    </div>
  );
}

export default App;
